﻿function onSignIn(googleUser) {
    // Useful data for your client-side scripts:
    var profile = googleUser.getBasicProfile();
    console.log("ID: " + profile.getId()); // Don't send this directly to your server!
    console.log('Full Name: ' + profile.getName());
    console.log('Given Name: ' + profile.getGivenName());
    console.log('Family Name: ' + profile.getFamilyName());
    console.log("Image URL: " + profile.getImageUrl());
    console.log("Email: " + profile.getEmail());

    // The ID token you need to pass to your backend:
    var id_token = googleUser.getAuthResponse().id_token;
    console.log("ID Token: " + id_token);

    var profile = googleUser.getBasicProfile();
    var idToken = googleUser.getAuthResponse().id_token;

    gapi.auth2.getAuthInstance().disconnect().then(function () {
        //Do stuff here after the user has been signed out, you can still authenticate the token with Google on the server side
    });


    GoogleRegister(
           googleUser.getAuthResponse().id_token
           , profile.getGivenName()
           , profile.getFamilyName()
           , profile.getEmail()
           );

};

function GoogleRegister(GoogleNumber, FirstName, LastName, Email) {
    var url = contextPath+"Register/Google?";

    if (GoogleNumber != null) {
        url += "GoogleNumber=" + GoogleNumber;
    }
    if (FirstName != null) {
        url += "&FirstName=" + FirstName;
    }
    if (LastName != null) {
        url += "&LastName=" + LastName;
    }
    if (Email != null) {
        url += "&Email=" + Email;
    }

    ajaxUrlRequest(url, GoogleRegisterResponse);
}

function GoogleRegisterResponse(result) {
    if (result.StatusCode == "0") {
        location.href = contextPath+"Home";
    }
}