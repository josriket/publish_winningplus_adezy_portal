﻿function logout() {
    FB.init({
        appId: FacebookAppId,
        cookie: true,
        xfbml: true,
        version: 'v2.11'
    });
    FB.logout();
}

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            console.log(response.authResponse.accessToken);

            testAPI();
        }
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: FacebookAppId,
        cookie: true,
        xfbml: true,
        version: 'v2.11' 
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI() {
    FB.api('/me?fields=email,first_name,last_name,name,id', function (response) {
        console.log("FB.api/me");
        console.log(response);
        console.log(response.first_name);
        console.log(response.last_name);
        console.log(response.name);
        console.log(response.id);
        console.log(response.email);
        FBRegister(
            response.id
            , response.first_name
            , response.last_name
            , response.email
            );
    });
}

function FBRegister(FacebookNumber,FirstName, LastName, Email) {
    var url = contextPath+"Register/Facebook?";

    if (FacebookNumber != null) {
        url += "FacebookNumber=" + FacebookNumber;
    }
    if (FirstName != null) {
        url += "&FirstName=" + FirstName;
    }
    if (LastName != null) {
        url += "&LastName=" + LastName;
    }
    if (Email != null) {
        url += "&Email=" + Email;
    }

    ajaxUrlRequest(url, FBRegisterResponse);
}

function FBRegisterResponse(result) {
    if (result.StatusCode == "0") {
        location.href = contextPath + "Home";
    }
}